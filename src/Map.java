

/**
 * @author Cetin Mericli
 *
 */
public class Map {
	static final int NEAR = 20;
	static final int MODERATE = 45;
	static final int FAR = 85;

	// Modify that function so that it reflects the map you will be using
	public int expectedDistance( int location ) {
		int ret = 0;

		if ( location >=0 && location < 27) {
			ret = NEAR;
		}
		else if ( location >=27 && location < 70 )	{
			ret = FAR;
		}
		else {
			ret = MODERATE;
		}
		return ret;
	}
}
