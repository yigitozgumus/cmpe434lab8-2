

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;

import javax.swing.JFrame;

public class WhereAmIPC extends JFrame {

	private static final long serialVersionUID = 2280874288993963333L;

	Map map;
	static MCL mcl;

	static InputStream inputStream;
	static DataInputStream dataInputStream;

	public WhereAmIPC() {
		super("Localization Monitor for CmpE 434");
		setSize( 700, 500 );
		setVisible( true );

		map = new Map();

		// Replace this with your selection of number of particles
		mcl = new MCL(100, map);

	}

	public static void main(String[] args) throws Exception	{

		int motion;
		int distance;

		WhereAmIPC monitor = new WhereAmIPC();

		monitor.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

		String ip = "192.168.1.218";

		@SuppressWarnings("resource")
		Socket socket = new Socket(ip,1234);
		System.out.println("Connected!");

		inputStream = socket.getInputStream();
		dataInputStream = new DataInputStream(inputStream);

		while( true ){
			motion = dataInputStream.readInt();
			distance = (int)(dataInputStream.readFloat()*100);
			mcl.motionUpdate(motion);
			mcl.measurementUpdate(distance);
			mcl.calculatePose();
			System.out.println(motion + " " + distance );
			monitor.repaint();
		}
	}


	public void paint( Graphics g ) {
		super.paint( g );
		displayMap( map, g);
		displayParticles( mcl.particles, g );
		displayPose( mcl.pose, g );
	}

	public void displayMap( Map map, Graphics g ){
		Graphics2D g2 = ( Graphics2D ) g;
		g2.setPaint( Color.BLUE );
		g2.setStroke( new BasicStroke( 5.0f ));

		for ( int i = 1; i < MCL.WORLD_SIZE; i++ ){
			g2.draw( new Line2D.Double( ( int ) ( ( i - 1 )*3 ) , 180 - map.expectedDistance( i - 1 )  , ( int ) ( i * 3 ), 180 - map.expectedDistance( i ) ) );
		}

	}

	public void displayParticles( Particle particles[], Graphics g ) {
		for ( int i = 0; i < mcl.NUM_PARTICLES ; i++ ){
			displayParticle( particles[ i ], g );
		}
	}
	public void displayParticle( Particle particle, Graphics g ){
		Graphics2D g2 = ( Graphics2D ) g;
		g2.setPaint( Color.red );
		g2.setStroke( new BasicStroke( 2.0f ));
		g2.draw( new Line2D.Double( ( int ) ( particle.location * 3 ), 300, ( int )( particle.location * 3 ), ( int )( 300 - 100*particle.belief ) ) );
	}

	public void displayPose( Particle particle, Graphics g ){
		Graphics2D g2 = ( Graphics2D ) g;
		g2.setPaint( Color.blue );
		g2.setStroke( new BasicStroke( 5.0f ));

		g2.draw( new Line2D.Double( ( int ) ( particle.location * 3 ), 300, ( int )( particle.location * 3 ), ( int )( 300 - 100*particle.belief ) ) );
	}

}

